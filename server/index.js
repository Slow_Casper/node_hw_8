const express = require('express');
const dotenv = require('dotenv');
const path = require('path');
const cors = require('cors');
const expressWinston = require('express-winston');
const { transports, format } = require('winston');

const staticGet = require('./routes/staticGet');
const newsPosts = require('./routes/newsPosts');

const initPath = path.resolve(__dirname, 'Files/Logs');
const errorLogFile = path.join(initPath, 'errorLogs.log');
const infoLogFile = path.join(initPath, 'infoLogs.log');
const warnLogFile = path.join(initPath, 'warningLogs.log');

dotenv.config();

const app = express();
const staticDirPath = path.resolve(__dirname, '../react_pages/build');
const defaultFormat = format.printf(({ meta, level, timestamp }) => {
    const { method, originalUrl } = meta.req;
    const loginfo = {
        time: timestamp,
        level,
        method,
        route: originalUrl
    };
    return JSON.stringify(loginfo, null, 2);
});

app.use(express.json());

app.use(express.static(staticDirPath));
app.use(cors({
    origin: process.env.REDIRECT_URL,
    methods: 'GET, POST, PUT, DELETE',
}));

app.use(expressWinston.logger({
    format: format.combine(format.json(), format.timestamp(), defaultFormat),
    transports: [
        new transports.Console(),
        new transports.File({ filename: errorLogFile, level: 'error' }),
        new transports.File({ filename: warnLogFile, level: 'warn' }),
        new transports.File({ filename: infoLogFile, level: 'info' })
    ],
    statusLevels: true,
}));

app.use('/api/newsposts', newsPosts);

app.use('*', staticGet);

const PORT = process.env.PORT || 3001;
app.listen(PORT, () => console.log(`server is running on port ${PORT}`));
