const fsPromise = require('fs/promises');
const path = require('path');

const newsDBPath = path.join(__dirname, '../Files', 'news.json');

exports.getAll = async (params) => {
    const { page, size } = params;

    const access = await fsPromise.access(newsDBPath).then(() => true).catch(() => false);
    if(access) {
        const newsDB = await fsPromise.readFile(newsDBPath, 'utf-8');
        const parsedNews = JSON.parse(newsDB);
        const paginationPost = parsedNews.slice(page * size - size, size * page);
        const resp = {
            posts: paginationPost,
            leftPosts: Math.max(parsedNews.length - (page * size), 0),
        };
        return resp;
    } else {
        await fsPromise.writeFile(newsDBPath, []);
        const newsDB = await fsPromise.readFile(newsDBPath, 'utf-8');
        return newsDB;
    }
};

exports.createPost = async (data) => {
    const id = Date.now();
    const { text, title } = data;

    const newPost = { id, title, text };

    const newsDB = await fsPromise.readFile(newsDBPath, 'utf-8');
    const parsedNews = JSON.parse(newsDB);
    
    parsedNews.push(newPost);
    
    await fsPromise.writeFile(newsDBPath, JSON.stringify(parsedNews, null, 2));
    return newPost;
};

exports.getById = async (id) => {
    const newsDB = await fsPromise.readFile(newsDBPath, 'utf-8');
    const parsedNews = JSON.parse(newsDB);

    return parsedNews.find((el) => el.id == id);
};

exports.updatePost = async (id, update) => {
    const newsDB = await fsPromise.readFile(newsDBPath, 'utf-8');
    const parsedNews = JSON.parse(newsDB);
    
    const currentPost = parsedNews.find((el) => el.id == id);
    if(currentPost) {
        Object.keys(update).forEach(key => currentPost[key] = update[key]);
        await fsPromise.writeFile(newsDBPath, JSON.stringify(parsedNews, null, 2));

        return currentPost;
    }
};

exports.delPost = async (id) => {
    const newsDB = await fsPromise.readFile(newsDBPath, 'utf-8');
    const parsedNews = JSON.parse(newsDB);

    const filteredPosts = parsedNews.filter((el) => el.id != id);

    if(filteredPosts) {
        await fsPromise.writeFile(newsDBPath, JSON.stringify(filteredPosts, null, 2));
        
        return filteredPosts
    };
};
