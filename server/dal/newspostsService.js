const { newspostsServiceError, validationError } = require('../services/Errors/Errors');
const { validateNewsPost } = require('../services/validation/validationSchema');
const { getAll, createPost, getById, updatePost, delPost } = require('./newsPostsRepository');

exports.getAll = async (params, res) => {
    try {
        const getPaginationNews = await getAll(params);
        res.status(200).send({
            success: true,
            data: getPaginationNews,
        });
    } catch (error) {
        res.status(500).json({
            message: newspostsServiceError(`Error happened on server: "${error}`)
        });
    };
};

exports.create = async (data, res) => {
    try {
        const validationResult = validateNewsPost(data)

        if(validationResult !== true) {
            res.status(400).json({
                message: validationError(validationResult),
            });
            return;
        } else {
            const newPost = await createPost(data);
            res.status(201).send({
                success: true,
                newPost: newPost,
            });
        }
    } catch (error) {
        res.status(500).json({
            message: newspostsServiceError(`Error happened on server: "${error}`)
        });
    };
};

exports.getById = async (id, res) => {
    try {
        const singlePost = await getById(id);
        if (singlePost) {
            res.status(200).send({
                success: true,
                currentPost: singlePost,
            });
        } else {
            res.status(400).json({
                message: newspostsServiceError(`Post with id ${id} no found`)
            });
        };
    } catch (error) {
        res.status(500).json({
            message: newspostsServiceError(`Error happened on server: "${error}`)
        });
    }
};

exports.update = async (id, update, res) => {
    try {

        const validationResult = validateNewsPost(update)

        if(validationResult !== true) {
            res.status(400).json({
                message: validationError(validationResult),
            });
            return;
        } else {
            const updatedPost = await updatePost(id, update)
            
            if(updatedPost) {
                res.status(200).send({
                    success: true,
                    updatedPost: updatedPost,
                });
            } else {
                res.status(400).json({
                    message: newspostsServiceError(`Post with id ${id} no found`)
                });
            }
        }
    } catch (error) {
        res.status(500).json({
            message: newspostsServiceError(`Error happened on server: "${error}`)
        });
    }
};

exports.delPost = async (id, res) => {
    try {
        const filteredPosts = await delPost(id);
        if (filteredPosts) {
            res.status(200).send({
                success: true,
                post: filteredPosts,
            });  
        } else {
            res.status(400).json({
                message: newspostsServiceError(`Post with id ${id} no found`)
            });
        }
    } catch (error) {
        res.status(500).json({
            message: `Error happened on server: "${error}`
        });
    }
};
