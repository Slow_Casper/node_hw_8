const expressWinston = require('express-winston');
const { transports, format } = require('winston');

const path = require('path');

const initPath = path.resolve(__dirname, 'Files/Logs');
const errorLogFile = path.join(initPath, 'errorLogs.log');
const infoLogFile = path.join(initPath, 'infoLogs.log');
const warnLogFile = path.join(initPath, 'warningLogs.log');

exports.logger = (err, req) => expressWinston.logger({
    format: format.combine(format.json(), format.timestamp(), format.prettyPrint()),
    transports: [
        new transports.Console(),
        new transports.File({ filename: errorLogFile, message: err.stack, level: 'error' }),
        new transports.File({ filename: warnLogFile, message: err.stack, level: 'warn' }),
        new transports.File({ filename: infoLogFile, message: `${req.method} ${req.originalUrl} - ${JSON.stringify(req.body)}`, level: 'info' })
    ],
    statusLevels: true,
});

exports.logHandler = (err, req) => {
    console.log(logger())
    // if(!err) {
    //     logger.info({
    //         message: `${req.method} ${req.originalUrl} - ${JSON.stringify(req.body)}`
    //     });
    // } else {
    //     logger.error({
    //         message: err.stack
    //     });
    // };
};
