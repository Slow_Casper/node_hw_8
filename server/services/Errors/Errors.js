exports.validationError = (data) => {
    return {name: 'Validation Error', message: data.map(el => el.message).join(', ')}
};

exports.newspostsServiceError = (message) => {
    return {name: 'Newsposts Service Error', message}
};
