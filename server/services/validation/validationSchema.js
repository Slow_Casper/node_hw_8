const Validator = require("fastest-validator");

const validator = new Validator();

const newspostSchema = {
    title: { type: "string", min: 10, max: 50 },
    text: { type: "string", min: 10, max: 256 },
    genre: { type: "string", enum: [ 'Politic', 'Business', 'Sport', 'Other' ] },
    isPrivate: { type: "boolean" },
};

exports.validateNewsPost = validator.compile(newspostSchema);
