import React, { useEffect, useState } from 'react';
import axios from 'axios';
import PostsList from '../../Components/Posts/PostsList/PostsList.tsx';
import { NavLink } from 'react-router-dom';
import Button from '../../Components/Button/Button.tsx';

const MainPage = () => {
  const [ posts, setPosts ] = useState([]);
  const [ nextPage, setNextPage ] = useState([]);
  const [ queryParams, setQueryParams ] = useState({
    page: 1,
    size: 3
  });

  useEffect(() => {
    const fetchData = async () => {
      try {
        await axios.get(`http://localhost:8000/api/newsposts?page=${queryParams.page}&size=${queryParams.size}`)
          .then(({ data }) => {if(data?.posts.length > 0) setPosts(data.posts)});

        await axios.get(`http://localhost:8000/api/newsposts?page=${queryParams.page + 1}&size=${queryParams.size}`)
          .then(({ data }) => setNextPage(data.posts));
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, [queryParams]);

  const prevNext = (param) => {
    if (param === 'next') {
      setQueryParams(prev => {
        return {...prev, page: prev.page + 1}
      })
    } else {
      setQueryParams(prev => {
        return {...prev, page: prev.page - 1}
      })
    }
  };

  return (
    <section
      style={{
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        gap: '20px'
      }}
    >
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'column',
          gap: '20px',
        }}
      >
        <h1>Welcome to the actual news</h1>
        <div style={{
          display: 'flex',
          gap: '20px'
        }}>
          <Button disabled={queryParams.page === 1} text='Prev page' action={() => prevNext('prev')} />
          <NavLink
            to='/newpost'
            style={{
              backgroundColor: 'gray',
              padding: '10px',
              textDecoration: 'none',
              color: 'black',
            }}
          >
            Add post
          </NavLink>
          <Button disabled={nextPage.length === 0} text='Next page' action={() => prevNext('next')} />
        </div>
      </div>
      <PostsList elementsPerPage={queryParams.size} posts={posts} />
    </section>
  );
};

export default MainPage;
