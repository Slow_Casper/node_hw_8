import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router';
import { NavLink } from 'react-router-dom'
import PostElement from '../../Components/Posts/PostElement/PostElement.tsx';

const SinglePostPage = () => {
  const [ post, setPost ] = useState();
  const [ error, setError ] = useState();
  const { newsId } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const { data } = await axios.get(`http://localhost:8000/api/newsposts/${newsId}`);
        // console.log(data)
        setPost(data.currentPost
          )
      } catch (error) {
        console.error('Error fetching data:', error);
        setError(error.response.data.message)
      }
    };

    fetchData();
  }, [newsId]);

  const deletePost = async () => {
    try {
      await axios.delete(`http://localhost:8000/api/newsposts/${newsId}`);
      navigate('/')
    } catch (error) {
      console.log(error)
    }
  };

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        gap: '50px'
      }}
    >
      <NavLink
        style={{
          backgroundColor: 'gray',
          padding: '10px',
          textDecoration: 'none',
          color: 'black',
        }}
        to='/'
      >
        Back to the news
      </NavLink>
      <NavLink
        style={{
          backgroundColor: 'gray',
          padding: '10px',
          textDecoration: 'none',
          color: 'black',
        }}
        to={`/editpost/${newsId}`}
      >
        Edit post
      </NavLink>
      <button
        style={{
          backgroundColor: 'gray',
          padding: '10px',
          textDecoration: 'none',
          color: 'black',
        }}
        onClick={deletePost}
      >
        Delete post
      </button>
      {post && <PostElement element={post} />}
      {error && <p>{error}</p>}
    </div>
  )
}

export default SinglePostPage
