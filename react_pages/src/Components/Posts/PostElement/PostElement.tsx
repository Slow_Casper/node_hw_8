import React from 'react'
import { Post } from '../PostsList/PostsList'
import { useNavigate } from 'react-router';

const PostElement = ({ element }: { element: Post }) => {
  const { id, title, text } = element;

  const navigate = useNavigate();

  const handleOnClick = () => {
    navigate(`/news/${id}`)
  };

  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        gap: '10px',
        border: '2px solid #000',
        borderRadius: '16px',
        padding: '5px'
      }}
      onClick={handleOnClick}
    >
      <p style={{ textAlign: 'center'}}>{title}</p>
      <p style={{
        display: '-webkit-box',
        textAlign: 'center',
        WebkitLineClamp: 3,
        WebkitBoxOrient: 'vertical',
        overflow: 'hidden',
      }}
      >
        {text}
      </p>
    </div>
  )
}

export default PostElement
